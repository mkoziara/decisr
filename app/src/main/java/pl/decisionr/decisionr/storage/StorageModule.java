package pl.decisionr.decisionr.storage;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.decisionr.decisionr.auth.FirebaseAuthModule;

@Module(includes = FirebaseAuthModule.class)
public class StorageModule {

    @Provides
    @Singleton
    public ImageStorage providesImageStorage(FirebaseStorage firebaseStorage, FirebaseAuth firebaseAuth) {
        return new ImageStorage(firebaseStorage, firebaseAuth);
    }

    @Provides
    @Singleton
    public FirebaseStorage providesFirebaseStorage() {
        return FirebaseStorage.getInstance();
    }

}
