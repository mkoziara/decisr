package pl.decisionr.decisionr.storage;

import android.net.Uri;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Date;

import javax.inject.Inject;

public class ImageStorage {

    private static final String STORAGE_URL = "gs://decisionr-fd7c2.appspot.com";
    private static final String JPG_EXTENSION = ".jpg";
    private static final String IMAGES_FOLDER = "images";

    private final FirebaseStorage firebaseStorage;
    private final FirebaseAuth firebaseAuth;

    @Inject
    public ImageStorage(FirebaseStorage firebaseStorage, FirebaseAuth firebaseAuth) {
        this.firebaseStorage = firebaseStorage;
        this.firebaseAuth = firebaseAuth;
    }

    public String saveImage(Uri uri) {
        // TODO: 2017-02-04 current user powinien być pewnie dostarczony przez jakiś wrapper
        String authId = firebaseAuth.getCurrentUser().getUid();
        StorageReference storage = firebaseStorage.getReferenceFromUrl(STORAGE_URL);

        String name = String.valueOf(new Date().getTime()) + JPG_EXTENSION ;
        storage.child(authId).child(IMAGES_FOLDER).child(name).putFile(uri);

        return String.format("%s/%s/%s", authId, IMAGES_FOLDER, name);
    }
}
