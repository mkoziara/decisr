package pl.decisionr.decisionr.core.paths;

public class FilesException extends RuntimeException {

    public FilesException(Throwable throwable) {
        super(throwable);
    }
}
