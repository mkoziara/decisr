package pl.decisionr.decisionr.core.checkable;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.Objects;

import pl.decisionr.decisionr.core.parcel.ParcelGenericConverter;

@Parcel
public class CheckableAdapterModel<T> {

    @ParcelPropertyConverter(ParcelGenericConverter.class)
    T model;
    boolean isChecked;

    public CheckableAdapterModel() {
    }

    public CheckableAdapterModel(T model) {
        this.model = model;
        this.isChecked = false;
    }

    public CheckableAdapterModel(T model, boolean isChecked) {
        this.model = model;
        this.isChecked = isChecked;
    }

    public T getModel() {
        return model;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void toggleChecked() {
        isChecked = !isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckableAdapterModel<?> that = (CheckableAdapterModel<?>) o;
        return Objects.equals(model, that.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model);
    }
}
