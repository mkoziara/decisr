package pl.decisionr.decisionr.core.fragment;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;

import javax.inject.Inject;

import pl.decisionr.decisionr.ApplicationComponent;
import pl.decisionr.decisionr.DecisionrApplication;
import pl.decisionr.decisionr.core.activity.ActivityModule;
import pl.decisionr.decisionr.core.activity.menu.MenuProcessor;
import pl.decisionr.decisionr.core.activity.result.ActivityResultProcessor;

public class BaseFragment extends Fragment {

    @Inject
    ActivityResultProcessor resultProcessor;
    @Inject
    MenuProcessor menuProcessor;

    protected ApplicationComponent getApplicationComponent() {
        return getApplication().getApplicationComponent();
    }

    protected ActivityModule createActivityModule() {
        return new ActivityModule((AppCompatActivity) getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                onBackPressed();
                return true;
            }

            return false;
        });
    }

    protected void onBackPressed(){};

    private DecisionrApplication getApplication() {
        return (DecisionrApplication) getActivity().getApplication();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        resultProcessor.processResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return menuProcessor.processMenuItem(item);
    }
}