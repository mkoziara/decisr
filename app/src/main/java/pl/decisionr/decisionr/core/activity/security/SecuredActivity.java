package pl.decisionr.decisionr.core.activity.security;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import pl.decisionr.decisionr.R;
import pl.decisionr.decisionr.core.activity.BaseActivity;
import pl.decisionr.decisionr.views.signin.SignInActivity;

public abstract class SecuredActivity extends BaseActivity {

    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser == null) {
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_signout) {
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(t -> {
                        startActivity(new Intent(this, SignInActivity.class));
                        finish();
                    });
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}