package pl.decisionr.decisionr.core.activity.menu;

import android.view.MenuItem;

import com.annimon.stream.Optional;

import java.util.Map;

import javax.inject.Inject;

public class MenuProcessor {

    private final Map<Integer, MenuItemProcessor> menuItemProcessors;

    @Inject
    public MenuProcessor(Map<Integer, MenuItemProcessor> menuItemProcessors) {
        this.menuItemProcessors = menuItemProcessors;
    }

    public boolean processMenuItem(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        Optional<MenuItemProcessor> processor = Optional.ofNullable(menuItemProcessors.get(itemId));

        if(processor.isPresent()) {
            processor.get().processMenuItem();
            return true;
        }

        return false;
    }
}