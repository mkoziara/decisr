package pl.decisionr.decisionr.core.paths;

import android.app.Application;
import android.os.Environment;

import java.io.File;

public class AndroidPathsProvider {

    private final Application application;

    public AndroidPathsProvider(Application application) {
        this.application = application;
    }

    public File getApplicationPicturesDir() {
        return application.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    }

    public File getApplicationDocumentsDir() {
        return application.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
    }
}
