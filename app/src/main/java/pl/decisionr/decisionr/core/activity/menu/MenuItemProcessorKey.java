package pl.decisionr.decisionr.core.activity.menu;

import android.support.annotation.IdRes;

import dagger.MapKey;

@MapKey
public @interface MenuItemProcessorKey {

    @IdRes
    int value();
}
