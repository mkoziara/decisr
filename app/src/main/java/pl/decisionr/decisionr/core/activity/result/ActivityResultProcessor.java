package pl.decisionr.decisionr.core.activity.result;


import android.app.Activity;
import android.content.Intent;

import com.annimon.stream.Optional;

import java.util.Map;

import javax.inject.Inject;

public class ActivityResultProcessor {

    private final Map<ResultProcessorType, ResultProcessor> resultProcessors;

    @Inject
    public ActivityResultProcessor(Map<ResultProcessorType, ResultProcessor> resultProcessors) {
        this.resultProcessors = resultProcessors;
    }

    public void processResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            processResult(resultProcessors.get(ResultProcessorType.OK), requestCode, data);
        } else if(resultCode == Activity.RESULT_CANCELED) {
            processResult(resultProcessors.get(ResultProcessorType.CANCEL), requestCode, data);
        }
    }

    private void processResult(ResultProcessor processor, int requestCode, Intent data) {
        Optional.ofNullable(processor).ifPresent(p -> p.processResult(requestCode, data));
    }
}