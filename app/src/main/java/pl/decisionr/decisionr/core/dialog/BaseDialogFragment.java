package pl.decisionr.decisionr.core.dialog;

import android.support.v4.app.DialogFragment;

import pl.decisionr.decisionr.ApplicationComponent;
import pl.decisionr.decisionr.DecisionrApplication;

public class BaseDialogFragment extends DialogFragment {

    public ApplicationComponent getApplicationComponent() {
        return getApplication().getApplicationComponent();
    }

    private DecisionrApplication getApplication() {
        return (DecisionrApplication) getActivity().getApplication();
    }
}
