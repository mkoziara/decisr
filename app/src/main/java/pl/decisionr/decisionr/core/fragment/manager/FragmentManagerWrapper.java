package pl.decisionr.decisionr.core.fragment.manager;

import android.os.Build;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.transition.Slide;
import android.view.Gravity;

import javax.inject.Inject;

public class FragmentManagerWrapper {

    private final FragmentManager fragmentManager;

    @Inject
    public FragmentManagerWrapper(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public void addFragment(@IdRes int layoutID, Fragment fragment) {
        fragment.setAllowEnterTransitionOverlap(false);
        fragment.setAllowReturnTransitionOverlap(false);

        prepareTransaction(layoutID, fragment)
                .commit();
    }

    public Fragment findFragmentByID(@IdRes int id) {
        return fragmentManager.findFragmentById(id);
    }

    public void addBackStackChangeCallback(FragmentManager.OnBackStackChangedListener callback) {
        fragmentManager.addOnBackStackChangedListener(callback);
    }

    public void removeBackStackChangeCallback(FragmentManager.OnBackStackChangedListener callback) {
        fragmentManager.removeOnBackStackChangedListener(callback);
    }

    private FragmentTransaction prepareTransaction(int layoutID, Fragment fragment) {
        FragmentTransaction transaction = fragmentManager
                .beginTransaction()
                .replace(layoutID, fragment);

        if (hasFragments()) {
            return transaction.addToBackStack(null);
        }

        return transaction;
    }

    public boolean hasFragments() {
        return fragmentManager.getFragments() != null && !fragmentManager.getFragments().isEmpty();
    }
}