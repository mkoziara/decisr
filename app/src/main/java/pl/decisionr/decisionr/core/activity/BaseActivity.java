package pl.decisionr.decisionr.core.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import javax.inject.Inject;

import butterknife.ButterKnife;
import pl.decisionr.decisionr.ApplicationComponent;
import pl.decisionr.decisionr.DecisionrApplication;
import pl.decisionr.decisionr.R;
import pl.decisionr.decisionr.core.activity.menu.MenuProcessor;
import pl.decisionr.decisionr.core.activity.result.ActivityResultProcessor;
import pl.decisionr.decisionr.core.toolbar.ToolbarModule;

public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    ActivityResultProcessor resultProcessor;
    @Inject
    MenuProcessor menuProcessor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeInjector();
    }

    protected abstract void initializeInjector();

    protected ApplicationComponent getApplicationComponent() {
        return ((DecisionrApplication) getApplication()).getApplicationComponent();
    }

    public ToolbarModule createToolbarModule() {
        Toolbar toolbar = ButterKnife.findById(this, R.id.toolbar);
        return new ToolbarModule(toolbar);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        resultProcessor.processResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return menuProcessor.processMenuItem(item);
    }
}