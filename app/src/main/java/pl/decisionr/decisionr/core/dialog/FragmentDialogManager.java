package pl.decisionr.decisionr.core.dialog;

import javax.inject.Inject;

import pl.decisionr.decisionr.core.fragment.BaseFragment;

public class FragmentDialogManager {

    private final BaseFragment baseFragment;

    @Inject
    public FragmentDialogManager(BaseFragment baseFragment) {
        this.baseFragment = baseFragment;
    }

    public void showDialog(BaseDialogFragment baseDialogFragment) {
        baseDialogFragment.show(baseFragment.getFragmentManager(), null);
    }
}
