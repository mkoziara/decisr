package pl.decisionr.decisionr.core.activity;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import java.util.Map;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.Multibinds;
import pl.decisionr.decisionr.core.activity.finish.ActivityFinisher;
import pl.decisionr.decisionr.core.activity.menu.MenuItemProcessor;
import pl.decisionr.decisionr.core.activity.menu.MenuProcessor;
import pl.decisionr.decisionr.core.activity.result.ActivityResultProcessor;
import pl.decisionr.decisionr.core.activity.result.ResultProcessor;
import pl.decisionr.decisionr.core.activity.result.ResultProcessorType;
import pl.decisionr.decisionr.core.fragment.manager.FragmentManagerWrapper;

@Module(includes = ActivityModule.ActivityOptionalModule.class)
public class ActivityModule {

    private AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    public AppCompatActivity providesActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    public ActivityStarter providesActivityStarter() {
        return new ActivityStarter(activity);
    }

    @Provides
    @ActivityScope
    public ActivityFinisher providesActivityFinisher() {
        return new ActivityFinisher(activity);
    }

    @Provides
    @ActivityScope
    public FragmentManagerWrapper providesFragmentManagerWrapper(FragmentManager fragmentManager) {
        return new FragmentManagerWrapper(fragmentManager);
    }

    @Provides
    @ActivityScope
    public FragmentManager providesFragmentManager() {
        return activity.getSupportFragmentManager();
    }

    @Provides
    @ActivityScope
    public ActivityResultProcessor providesActivityResultProcessor(Map<ResultProcessorType, ResultProcessor> resultProcessors) {
        return new ActivityResultProcessor(resultProcessors);
    }

    @Provides
    @ActivityScope
    public MenuProcessor providesMenuProcessor(Map<Integer, MenuItemProcessor> menuItemProcessors) {
        return new MenuProcessor(menuItemProcessors);
    }

    @Module
    public abstract class ActivityOptionalModule {
        @Multibinds
        abstract Map<ResultProcessorType, ResultProcessor> resultProcessors();

        @Multibinds
        abstract Map<Integer, MenuItemProcessor> menuItemProcessors();
    }

}