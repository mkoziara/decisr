package pl.decisionr.decisionr.core.parcel;

import android.os.Parcel;

import org.parceler.Parcels;
import org.parceler.converter.NullableParcelConverter;

public class ParcelGenericConverter extends NullableParcelConverter<Object> {

    @Override
    public void nullSafeToParcel(Object input, Parcel parcel) {
        parcel.writeParcelable(Parcels.wrap(input), 0);
    }

    @Override
    public Object nullSafeFromParcel(Parcel parcel) {
        return Parcels.unwrap(parcel.readParcelable(ParcelGenericConverter.class.getClassLoader()));
    }
}
