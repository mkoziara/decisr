package pl.decisionr.decisionr.core.activity.menu;

public interface MenuItemProcessor {

    void processMenuItem();

}
