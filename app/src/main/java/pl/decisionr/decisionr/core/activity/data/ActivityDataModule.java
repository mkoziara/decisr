package pl.decisionr.decisionr.core.activity.data;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import pl.decisionr.decisionr.core.activity.ActivityScope;

@Module
public class ActivityDataModule {

    private final AppCompatActivity activity;

    public ActivityDataModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    public Bundle providesExtras() {
        return activity.getIntent().getExtras();
    }
}
