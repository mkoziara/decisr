package pl.decisionr.decisionr.core.activity.finish;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

public class ActivityFinisher {

    private final AppCompatActivity appCompatActivity;

    @Inject
    public ActivityFinisher(AppCompatActivity appCompatActivity) {
        this.appCompatActivity = appCompatActivity;
    }

    public void finishActivityWithResult(Intent result) {
        appCompatActivity.setResult(Activity.RESULT_OK, result);
        appCompatActivity.finish();
    }
}