package pl.decisionr.decisionr.core.fragment;

import android.content.Intent;
import android.support.v4.app.Fragment;

public class FragmentActivityStarter {

    private final Fragment fragment;

    public FragmentActivityStarter(Fragment fragment) {
        this.fragment = fragment;
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        fragment.startActivityForResult(intent, requestCode);
    }
}
