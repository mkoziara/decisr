package pl.decisionr.decisionr.core.fragment;

import android.os.Bundle;

import dagger.Module;
import dagger.Provides;
import pl.decisionr.decisionr.core.activity.ActivityScope;
import pl.decisionr.decisionr.core.dialog.FragmentDialogManager;

@Module
public class FragmentModule {

    private final BaseFragment baseFragment;

    public FragmentModule(BaseFragment baseFragment) {
        this.baseFragment = baseFragment;
    }

    @Provides
    @ActivityScope
    public FragmentActivityStarter providesFragmentActivityStarter() {
        return new FragmentActivityStarter(baseFragment);
    }

    @Provides
    @ActivityScope
    public FragmentDialogManager providesFragmentDialogManager() {
        return new FragmentDialogManager(baseFragment);
    }

    @Provides
    @ActivityScope
    public Bundle providesArguments() {
        return baseFragment.getArguments();
    }
}