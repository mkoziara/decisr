package pl.decisionr.decisionr.core.toolbar;

import android.support.v7.widget.Toolbar;

import dagger.Module;
import dagger.Provides;
import pl.decisionr.decisionr.core.activity.ActivityScope;

@Module
public class ToolbarModule {

    private final Toolbar toolbar;

    public ToolbarModule(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    @Provides
    @ActivityScope
    public ToolbarManager providesToolbarManager() {
        return new ToolbarManager(toolbar);
    }
}
