package pl.decisionr.decisionr.core.notifications;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;

import java.util.Random;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PendingIntentFactory {

    private final Application application;

    @Inject
    public PendingIntentFactory(Application application) {
        this.application = application;
    }

    public PendingIntent createPendingIntent(Intent intent) {
        int randomRequestCode = new Random().nextInt(1024);

        return PendingIntent.getActivity(application, randomRequestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
