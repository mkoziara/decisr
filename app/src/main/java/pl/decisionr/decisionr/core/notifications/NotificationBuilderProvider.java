package pl.decisionr.decisionr.core.notifications;

import android.app.Application;
import android.support.v4.app.NotificationCompat;

import javax.inject.Inject;

public class NotificationBuilderProvider {

    private final Application application;

    @Inject
    public NotificationBuilderProvider(Application application) {
        this.application = application;
    }

    public NotificationCompat.Builder getNewBuilder() {
        return new NotificationCompat.Builder(application);
    }
}