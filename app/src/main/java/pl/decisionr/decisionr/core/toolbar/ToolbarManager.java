package pl.decisionr.decisionr.core.toolbar;

import android.support.annotation.DrawableRes;
import android.support.annotation.MenuRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.annimon.stream.function.Consumer;

import java.util.Objects;

import javax.inject.Inject;

public class ToolbarManager {

    private final Toolbar toolbar;

    @Inject
    public ToolbarManager(Toolbar toolbar) {
        Objects.requireNonNull(toolbar);
        this.toolbar = toolbar;
    }

    public void updateMenu(@MenuRes int menuId) {
        clearMenu();
        toolbar.inflateMenu(menuId);
    }

    public void addNavigationIcon(@DrawableRes int drawableId, Consumer<View> consumer) {
        toolbar.setNavigationIcon(drawableId);
        toolbar.setNavigationOnClickListener(consumer::accept);
    }

    public void clearMenu() {
        toolbar.getMenu().clear();
        toolbar.setNavigationIcon(null);
    }

    public void setTitle(String title) {
        toolbar.setTitle(title);
    }

    public void setTitle(@StringRes int title) {
        toolbar.setTitle(title);
    }
}