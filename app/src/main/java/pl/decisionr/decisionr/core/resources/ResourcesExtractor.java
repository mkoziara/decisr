package pl.decisionr.decisionr.core.resources;

import android.content.res.Resources;
import android.support.annotation.StringRes;

import javax.inject.Inject;

public class ResourcesExtractor {

    private final Resources resources;

    @Inject
    public ResourcesExtractor(Resources resources) {
        this.resources = resources;
    }

    public String getString(@StringRes int stringId) {
        return resources.getString(stringId);
    }
}
