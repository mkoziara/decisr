package pl.decisionr.decisionr.core.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

public class ActivityStarter {

    private final AppCompatActivity activity;

    @Inject
    public ActivityStarter(AppCompatActivity activity) {
        this.activity = activity;
    }

    public void startActivity(Intent intent) {
        activity.startActivity(intent);
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        activity.startActivityForResult(intent, requestCode);
    }
}
