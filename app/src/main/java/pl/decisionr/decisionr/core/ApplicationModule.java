package pl.decisionr.decisionr.core;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.decisionr.decisionr.core.notifications.NotificationBuilderProvider;
import pl.decisionr.decisionr.core.notifications.NotificationManagerWrapper;
import pl.decisionr.decisionr.core.notifications.PendingIntentFactory;
import pl.decisionr.decisionr.core.paths.AndroidPathsProvider;
import pl.decisionr.decisionr.core.resources.ResourcesExtractor;

@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public AndroidPathsProvider providesAndroidPathsProvider() {
        return new AndroidPathsProvider(application);
    }

    @Provides
    @Singleton
    public NotificationManagerWrapper providesNotificationManagerWrapper() {
        NotificationManager notificationmanager = (NotificationManager) application.getSystemService(Context.NOTIFICATION_SERVICE);
        return new NotificationManagerWrapper(notificationmanager);
    }

    @Provides
    @Singleton
    public ResourcesExtractor providesResourcesExtractor() {
        return new ResourcesExtractor(application.getResources());
    }

    @Provides
    @Singleton
    public PendingIntentFactory providesPendingIntentFactory() {
        return new PendingIntentFactory(application);
    }

    @Provides
    @Singleton
    public NotificationBuilderProvider providesNotificationBuilderProvider() {
        return new NotificationBuilderProvider(application);
    }
}
