package pl.decisionr.decisionr.core.activity;

import android.os.Bundle;

import com.annimon.stream.Optional;

import org.parceler.Parcels;

public class ActivityState {

    private final Optional<Bundle> bundle;

    public ActivityState() {
        this.bundle = Optional.of(new Bundle());
    }

    public ActivityState(Bundle bundle) {
        this.bundle = Optional.ofNullable(bundle);
    }

    public boolean hasState() {
        return bundle.isPresent();
    }

    public ActivityState putInt(String key, Integer value) {
        bundle.ifPresent(b -> b.putInt(key, value));
        return this;
    }

    public Optional<Integer> getInt(String key) {
        return bundle.map(b -> b.getInt(key));
    }

    public <T> ActivityState putParcel(String key, T value) {
        bundle.ifPresent(b -> b.putParcelable(key, Parcels.wrap(value)));
        return this;
    }

    public <T> Optional<T> getParcel(String key) {
        return bundle.map(b ->  Parcels.unwrap(b.getParcelable(key)));
    }

    public ActivityState putString(String key, String value) {
        bundle.ifPresent(b -> b.putString(key, value));
        return this;
    }

    public Optional<String> getString(String key) {
        return bundle.map(b -> b.getString(key));
    }

    public Bundle getBundle() {
        return bundle.orElse(new Bundle());
    }
}
