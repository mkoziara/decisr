package pl.decisionr.decisionr.core.activity.result;

import dagger.MapKey;

@MapKey
public @interface ResultProcessorKey {
    ResultProcessorType value();
}
