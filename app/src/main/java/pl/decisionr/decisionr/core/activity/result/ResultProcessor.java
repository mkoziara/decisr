package pl.decisionr.decisionr.core.activity.result;

import android.content.Intent;

public interface ResultProcessor {

    void processResult(int requestCode, Intent data);
}
