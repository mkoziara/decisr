package pl.decisionr.decisionr.core.notifications;

import android.app.Notification;
import android.app.NotificationManager;

import java.util.Random;

import javax.inject.Inject;

public class NotificationManagerWrapper {

    private final NotificationManager notificationManager;

    @Inject
    public NotificationManagerWrapper(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    public void notify(Notification notification) {
        int randomId = new Random().nextInt(1024);
        notificationManager.notify(randomId, notification);
    }
}
