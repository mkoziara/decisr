package pl.decisionr.decisionr;

import javax.inject.Singleton;

import dagger.Component;
import pl.decisionr.decisionr.auth.FirebaseAuthModule;
import pl.decisionr.decisionr.core.ApplicationModule;
import pl.decisionr.decisionr.core.paths.AndroidPathsProvider;
import pl.decisionr.decisionr.core.resources.ResourcesExtractor;
import pl.decisionr.decisionr.database.DatabaseModule;
import pl.decisionr.decisionr.database.decision.DecisionsRepository;
import pl.decisionr.decisionr.rest.DecisionsClient;
import pl.decisionr.decisionr.rest.RestModule;
import pl.decisionr.decisionr.storage.ImageStorage;
import pl.decisionr.decisionr.storage.StorageModule;

@Singleton
@Component(modules = {ApplicationModule.class, RestModule.class, DatabaseModule.class, FirebaseAuthModule.class, StorageModule.class})
public interface ApplicationComponent {

    AndroidPathsProvider androidPathsProvider();

    ResourcesExtractor resourcesExtractor();

    DecisionsRepository decisionRepository();

    ImageStorage imageStorage();

    DecisionsClient decisionsClient();
}
