package pl.decisionr.decisionr.rest;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import pl.decisionr.decisionr.database.decision.model.answer.DecisionAnswer;
import pl.decisionr.decisionr.database.decision.model.decision.Decision;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DecisionsClient {

    @POST("decisions")
    Observable<Decision> createNewDecision(@Body Decision decision);

    @GET("decisions")
    Observable<List<Decision>> getMyDecisions();

    @GET("otherDecisions")
    Observable<List<Decision>> getOtherDecisions();

    @POST("decisions/{id}/answer")
    Completable addAnswer(@Path("id") String id, @Body DecisionAnswer answer);

}