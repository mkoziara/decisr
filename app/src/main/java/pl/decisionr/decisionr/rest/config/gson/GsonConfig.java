package pl.decisionr.decisionr.rest.config.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.aaronhe.threetengson.ThreeTenGsonAdapter;

import pl.decisionr.decisionr.database.decision.model.answer.DecisionAnswer;
import pl.decisionr.decisionr.database.decision.model.answer.SimpleChoiceAnswer;
import pl.decisionr.decisionr.database.decision.model.decision.Decision;
import pl.decisionr.decisionr.database.decision.model.decision.DecisionType;
import pl.decisionr.decisionr.database.decision.model.decision.choice.SimpleChoiceDecision;

public class GsonConfig {

    public Gson configureGson() {
        RuntimeTypeAdapterFactory<Decision> decisionTypeFactory = RuntimeTypeAdapterFactory.of(Decision.class, "type")
                .registerSubtype(SimpleChoiceDecision.class, DecisionType.SIMPLE_CHOICE.name());

        RuntimeTypeAdapterFactory<DecisionAnswer> answerTypeFactory = RuntimeTypeAdapterFactory.of(DecisionAnswer.class, "decisionAnswerType")
                .registerSubtype(SimpleChoiceAnswer.class, DecisionType.SIMPLE_CHOICE.name());

        GsonBuilder gsonBuilder = new GsonBuilder();
        ThreeTenGsonAdapter.registerLocalDateTime(gsonBuilder);

        return gsonBuilder
                .registerTypeAdapterFactory(decisionTypeFactory)
                .registerTypeAdapterFactory(answerTypeFactory)
                .create();
    }
}
