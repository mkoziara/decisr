package pl.decisionr.decisionr.rest.config;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import pl.decisionr.decisionr.rest.config.okhttp.JwtAuthorizationInterceptor;

public class OkHttpConfig {

    public OkHttpClient configureOkHttp() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(new JwtAuthorizationInterceptor())
                .build();
    }


}
