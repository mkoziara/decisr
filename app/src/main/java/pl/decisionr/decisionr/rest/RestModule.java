package pl.decisionr.decisionr.rest;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import pl.decisionr.decisionr.rest.config.OkHttpConfig;
import pl.decisionr.decisionr.rest.config.RetrofitConfig;
import pl.decisionr.decisionr.rest.config.gson.GsonConfig;
import retrofit2.Retrofit;

@Module
public class RestModule {

    @Provides
    @Singleton
    public Retrofit providesRetrofit() {
        OkHttpClient okHttpClient = new OkHttpConfig().configureOkHttp();
        Gson gson = new GsonConfig().configureGson();

        return new RetrofitConfig(okHttpClient, gson).configureRetrofit();
    }

    @Provides
    @Singleton
    public DecisionsClient providesDecisionsClient(Retrofit retrofit) {
        return retrofit.create(DecisionsClient.class);
    }
}