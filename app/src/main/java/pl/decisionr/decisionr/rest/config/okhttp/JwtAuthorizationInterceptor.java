package pl.decisionr.decisionr.rest.config.okhttp;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GetTokenResult;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class JwtAuthorizationInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Task<GetTokenResult> getTokenTask = FirebaseAuth.getInstance().getCurrentUser().getToken(true);

        while(true) {
            if (getTokenTask.isComplete()) break;
        }

        if (getTokenTask.isSuccessful()) {
            String authToken = getTokenTask.getResult().getToken();
            Request newRequest = chain.request().newBuilder()
                    .header("Authorization", "Bearer " + authToken)
                    .build();

            return chain.proceed(newRequest);
        }

        return chain.proceed(chain.request());
    }
}
