package pl.decisionr.decisionr.views.main.decisions.other;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.annimon.stream.Optional;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Action;
import pl.decisionr.decisionr.R;
import pl.decisionr.decisionr.core.activity.ActivityState;
import pl.decisionr.decisionr.core.fragment.BaseFragment;
import pl.decisionr.decisionr.database.decision.DecisionsRepository;
import pl.decisionr.decisionr.database.decision.model.decision.Decision;
import pl.decisionr.decisionr.database.decision.model.decision.choice.SimpleChoiceDecision;
import pl.decisionr.decisionr.views.main.DaggerMainComponent;

public class OthersDecisionsFragment extends BaseFragment {

    public static final String OTHER_DECISIONS_STATE = OthersDecisionsFragment.class.getCanonicalName() + ":OTHERS_LIST_STATE";

    @BindView(R.id.empty_decisions)
    View emptyDecisionsView;
    @BindView(R.id.simple_decision)
    View simpleChoiceDecisionView;

    @Inject
    DecisionsRepository decisionsRepository;

    private List<Decision> decisionList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_others_decisions, container, false);
        ButterKnife.bind(this, view);

        DaggerMainComponent
                .builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(createActivityModule())
                .build()
                .inject(this);

        // TODO: 2017-02-22 takie rzeczy nie powinny się wykonywać jeśli użytkownik nie jest zalogowany, powinno to być bardziej generyczne
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {

            ActivityState state = new ActivityState(savedInstanceState);
            if (state.hasState()) {
                restoreState(state);
            } else {
                initialize();
            }
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        ActivityState activityState = new ActivityState()
                .putParcel(OTHER_DECISIONS_STATE, decisionList);
        outState.putAll(activityState.getBundle());
        super.onSaveInstanceState(outState);
    }


    // TODO: 2017-02-26 presenter
    private void initialize() {
        decisionsRepository.getOtherDecisions()
                .map(r -> decisionList = r)
                .subscribe(r -> showNewDecision());
    }

    private void restoreState(ActivityState state) {
        Optional<List<Decision>> decisions = state.getParcel(OTHER_DECISIONS_STATE);
        decisions
                .map(r -> decisionList = r)
                .ifPresent(r -> showNewDecision());
    }

    private void showNewDecision() {
        if (decisionList.isEmpty()) {
            emptyDecisionsView.setVisibility(View.VISIBLE);
            simpleChoiceDecisionView.setVisibility(View.GONE);
            return;
        }

        Decision decision = decisionList.get(0);
        if (decision instanceof SimpleChoiceDecision) {
            emptyDecisionsView.setVisibility(View.GONE);
            simpleChoiceDecisionView.setVisibility(View.VISIBLE);

            Action addedAnswerConsumer = () -> {
                decisionList.remove(0);
                showNewDecision();
            };
            new SimpleChoiceDecisionPresenter(getActivity(), decisionsRepository, addedAnswerConsumer)
                    .fillSimpleChoiceDecisionData((SimpleChoiceDecision) decision);
        }
    }
}