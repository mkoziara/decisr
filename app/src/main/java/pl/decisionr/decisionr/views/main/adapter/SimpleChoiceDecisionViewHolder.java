package pl.decisionr.decisionr.views.main.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.decisionr.decisionr.R;

class SimpleChoiceDecisionViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.decision_title)
    TextView titleText;
    @BindView(R.id.decision_first_image)
    ImageView firstImageView;
    @BindView(R.id.decision_second_image)
    ImageView secondImageView;
    @BindView(R.id.left_answers_counter)
    TextView leftAnswersCounter;
    @BindView(R.id.right_answers_counter)
    TextView rightAnswersCounter;
    @BindView(R.id.decision_timer)
    TextView decisionTimer;

    SimpleChoiceDecisionViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
