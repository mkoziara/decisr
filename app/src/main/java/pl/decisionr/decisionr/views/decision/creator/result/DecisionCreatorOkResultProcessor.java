package pl.decisionr.decisionr.views.decision.creator.result;

import android.content.Intent;

import javax.inject.Inject;

import pl.decisionr.decisionr.core.activity.result.ResultProcessor;
import pl.decisionr.decisionr.views.decision.creator.DecisionCreatorView;
import pl.decisionr.decisionr.views.decision.creator.picture.ImageTempPathProvider;

import static pl.decisionr.decisionr.views.decision.creator.presenter.DecisionCreatorPresenter.FIRST_IMAGE_REQUEST;
import static pl.decisionr.decisionr.views.decision.creator.presenter.DecisionCreatorPresenter.SECOND_IMAGE_REQUEST;

public class DecisionCreatorOkResultProcessor implements ResultProcessor {

    private final DecisionCreatorView decisionCreatorView;
    private final ImageTempPathProvider imageTempPathProvider;

    @Inject
    public DecisionCreatorOkResultProcessor(DecisionCreatorView decisionCreatorView, ImageTempPathProvider imageTempPathProvider) {
        this.decisionCreatorView = decisionCreatorView;
        this.imageTempPathProvider = imageTempPathProvider;
    }

    @Override
    public void processResult(int requestCode, Intent data) {

        // TODO: 2017-02-04 walidować optional
        if (requestCode == FIRST_IMAGE_REQUEST) {
            decisionCreatorView.setFirstImage(imageTempPathProvider.getFirstImageUri().get());
        } else if (requestCode == SECOND_IMAGE_REQUEST) {
            decisionCreatorView.setSecondImage(imageTempPathProvider.getSecondImageUri().get());
        }
    }
}