package pl.decisionr.decisionr.views.decision.creator;

import android.net.Uri;

public interface DecisionCreatorView {

    void setFirstImage(Uri uri);
    void setSecondImage(Uri uri);

}
