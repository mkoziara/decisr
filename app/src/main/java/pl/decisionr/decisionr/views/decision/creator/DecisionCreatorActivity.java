package pl.decisionr.decisionr.views.decision.creator;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.decisionr.decisionr.R;
import pl.decisionr.decisionr.core.activity.ActivityModule;
import pl.decisionr.decisionr.core.activity.ActivityState;
import pl.decisionr.decisionr.core.activity.security.SecuredActivity;
import pl.decisionr.decisionr.database.decision.model.decision.DecisionState;
import pl.decisionr.decisionr.views.decision.creator.dagger.DecisionCreatorActivityModule;
import pl.decisionr.decisionr.views.decision.creator.presenter.DecisionCreatorPresenter;


public class DecisionCreatorActivity extends SecuredActivity implements DecisionCreatorView {

    public static final String CREATED_DECISION_EXTRA = DecisionCreatorActivity.class.getCanonicalName() + ":CREATED_DECISION";

    @BindView(R.id.decision_title)
    EditText editTitle;
    @BindView(R.id.decision_first_image)
    ImageView firstImageView;
    @BindView(R.id.decision_second_image)
    ImageView secondImageView;

    @BindView(R.id.state_infinite_time)
    RadioButton infiniteTimeButton;
    @BindView(R.id.state_time_to_live)
    RadioButton timeToLiveButton;

    @Inject
    DecisionCreatorPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decision_maker);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        ActivityState state = new ActivityState(savedInstanceState);
        if (state.hasState()) {
            presenter.restoreState(state);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        ActivityState state = presenter.getState();
        outState.putAll(state.getBundle());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void initializeInjector() {
        DaggerDecisionCreatorComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .decisionCreatorActivityModule(new DecisionCreatorActivityModule(this))
                .build()
                .inject(this);
    }

    @OnClick(R.id.decision_first_image)
    public void makeFirstImage() {
        presenter.makeFirstPicture();
    }

    @OnClick(R.id.decision_second_image)
    public void makeSecondImage() {
        presenter.makeSecondPicture();
    }

    @OnClick(R.id.fab)
    public void onFabClick() {
        String title = editTitle.getText().toString();
        presenter.saveDecision(title, getState());
    }

    private DecisionState getState() {
        if (infiniteTimeButton.isChecked()) {
            return DecisionState.INFINITE_TIME;
        }
        if (timeToLiveButton.isChecked()) {
            return DecisionState.TIME_TO_LIVE;
        }

        throw new IllegalStateException("Unable to detemine state of newly created decision.");
    }

    @Override
    public void setFirstImage(Uri uri) {
        setImage(uri, firstImageView);
    }

    @Override
    public void setSecondImage(Uri uri) {
        setImage(uri, secondImageView);
    }

    private void setImage(Uri uri, ImageView imageView) {
        Glide.with(this)
                .load(uri)
                .centerCrop()
                .into(imageView);
    }
}