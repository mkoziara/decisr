package pl.decisionr.decisionr.views.main.decisions.other;

import android.app.Activity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.functions.Action;
import pl.decisionr.decisionr.R;
import pl.decisionr.decisionr.database.decision.DecisionsRepository;
import pl.decisionr.decisionr.database.decision.model.answer.DecisionAnswer;
import pl.decisionr.decisionr.database.decision.model.answer.SimpleChoiceAnswer;
import pl.decisionr.decisionr.database.decision.model.decision.Decision;
import pl.decisionr.decisionr.database.decision.model.decision.choice.SimpleChoiceDecision;

public class SimpleChoiceDecisionPresenter {

    @BindView(R.id.others_decision_title)
    TextView titleTextView;
    @BindView(R.id.others_decision_first_image)
    ImageView firstImage;
    @BindView(R.id.others_decision_second_image)
    ImageView secondImage;

    private final DecisionsRepository decisionsRepository;
    private final Action addDecisionCallback;

    private SimpleChoiceDecision decision;

    public SimpleChoiceDecisionPresenter(Activity activity, DecisionsRepository decisionsRepository, Action addDecisionCallback) {
        this.decisionsRepository = decisionsRepository;
        this.addDecisionCallback = addDecisionCallback;

        ButterKnife.bind(this, activity);
    }

    public void fillSimpleChoiceDecisionData(SimpleChoiceDecision decision) {
        this.decision = decision;
        titleTextView.setText(decision.getTitle());

        StorageReference reference = FirebaseStorage.getInstance().getReference(decision.getFirstImagePath());
        Glide.with(firstImage.getContext())
                .using(new FirebaseImageLoader())
                .load(reference)
                .override(150, 150)
                .centerCrop()
                .into(firstImage);

        StorageReference second = FirebaseStorage.getInstance().getReference(decision.getSecondImagePath());
        Glide.with(secondImage.getContext())
                .using(new FirebaseImageLoader())
                .load(second)
                .override(150, 150)
                .centerCrop()
                .into(secondImage);
    }


    @OnClick(R.id.left_button)
    void addLeftAnswer() {
        SimpleChoiceAnswer left = SimpleChoiceAnswer.createLeft();
        addAnswer(decision, left);
    }

    @OnClick(R.id.right_button)
    void addRightAnswer() {
        SimpleChoiceAnswer right = SimpleChoiceAnswer.createRight();
        addAnswer(decision, right);
    }

    private void addAnswer(Decision decision, DecisionAnswer decisionAnswer) {
        decisionAnswer.setAnsweredBy(FirebaseAuth.getInstance().getCurrentUser().getUid());

        decisionsRepository.addAnswerToDecision(decision, decisionAnswer)
                .subscribe(addDecisionCallback::run);
    }
}