package pl.decisionr.decisionr.views.main.result;

import android.content.Intent;

import org.parceler.Parcels;

import javax.inject.Inject;

import pl.decisionr.decisionr.core.activity.result.ResultProcessor;
import pl.decisionr.decisionr.database.decision.model.decision.Decision;
import pl.decisionr.decisionr.views.decision.creator.DecisionCreatorActivity;
import pl.decisionr.decisionr.views.main.MainActivity;
import pl.decisionr.decisionr.views.main.decisions.my.MyDecisionsFragmentView;

public class MainOkResultProcessor implements ResultProcessor {

    private final MyDecisionsFragmentView view;

    @Inject
    public MainOkResultProcessor(MyDecisionsFragmentView view) {
        this.view = view;
    }

    @Override
    public void processResult(int requestCode, Intent data) {

        if (requestCode == MainActivity.DECISION_CREATE_REQUEST) {
            Decision newDecision = Parcels.unwrap(data.getParcelableExtra(DecisionCreatorActivity.CREATED_DECISION_EXTRA));
            view.addNewDecision(newDecision);
        }
    }
}