package pl.decisionr.decisionr.views.decision.creator;

import dagger.Component;
import pl.decisionr.decisionr.ApplicationComponent;
import pl.decisionr.decisionr.core.activity.ActivityScope;
import pl.decisionr.decisionr.views.decision.creator.dagger.DecisionCreatorActivityModule;

@Component(modules = DecisionCreatorActivityModule.class, dependencies = ApplicationComponent.class)
@ActivityScope
public interface DecisionCreatorComponent {

    void inject(DecisionCreatorActivity activity);


}
