package pl.decisionr.decisionr.views.decision.creator.presenter;

import android.net.Uri;

import org.threeten.bp.LocalDateTime;

import javax.inject.Inject;

import pl.decisionr.decisionr.database.decision.model.decision.DecisionState;
import pl.decisionr.decisionr.database.decision.model.decision.choice.SimpleChoiceDecision;
import pl.decisionr.decisionr.storage.ImageStorage;
import pl.decisionr.decisionr.views.decision.creator.picture.ImageTempPathProvider;

public class DecisionFactory {

    private final ImageStorage imageStorage;
    private final ImageTempPathProvider imageTempPathProvider;

    @Inject
    public DecisionFactory(ImageStorage imageStorage, ImageTempPathProvider imageTempPathProvider) {
        this.imageStorage = imageStorage;
        this.imageTempPathProvider = imageTempPathProvider;
    }

    public SimpleChoiceDecision createSimpleChoiceDecision(String title, DecisionState state) {

        // TODO: 2017-02-04 walidować optional
        Uri firstTempUri = imageTempPathProvider.getFirstImageUri().get();
        Uri secondTempUri = imageTempPathProvider.getSecondImageUri().get();

        String firstPath = imageStorage.saveImage(firstTempUri);
        String secondPath = imageStorage.saveImage(secondTempUri);

        SimpleChoiceDecision simpleChoiceDecision = new SimpleChoiceDecision(title, firstPath, secondPath);
        simpleChoiceDecision.setDecisionState(state);

        if (state.equals(DecisionState.TIME_TO_LIVE)) {
            simpleChoiceDecision.setFinishTime(LocalDateTime.now().plusDays(1));
        }

        return simpleChoiceDecision;
    }
}
