package pl.decisionr.decisionr.views.decision.creator.picture;

import java.io.File;
import java.util.Date;

import pl.decisionr.decisionr.core.paths.AndroidPathsProvider;

public class ImagePathProvider {

    public static final String PNG_EXTENSION = ".png";
    private final AndroidPathsProvider pathsProvider;

    public ImagePathProvider(AndroidPathsProvider pathsProvider) {
        this.pathsProvider = pathsProvider;
    }

    public File generatePathForImage() {

        File externalFilesDir = pathsProvider.getApplicationPicturesDir();
        String imageFileName = String.valueOf(new Date().getTime()) + PNG_EXTENSION;

        return new File(externalFilesDir, imageFileName);
    }
}
