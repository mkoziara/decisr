package pl.decisionr.decisionr.views.main;

import dagger.Component;
import pl.decisionr.decisionr.ApplicationComponent;
import pl.decisionr.decisionr.core.activity.ActivityModule;
import pl.decisionr.decisionr.core.activity.ActivityScope;
import pl.decisionr.decisionr.views.main.decisions.other.OthersDecisionsFragment;

@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
@ActivityScope
public interface MainComponent {

    void inject(MainActivity mainActivity);

    void inject(OthersDecisionsFragment othersDecisionsFragment);
}
