package pl.decisionr.decisionr.views.decision.creator.presenter;

import android.content.Intent;
import android.os.Parcelable;

import org.parceler.Parcels;

import javax.inject.Inject;

import pl.decisionr.decisionr.core.activity.finish.ActivityFinisher;
import pl.decisionr.decisionr.database.decision.model.decision.Decision;

import static pl.decisionr.decisionr.views.decision.creator.DecisionCreatorActivity.CREATED_DECISION_EXTRA;

public class DecisionCreatorActivityFinisher {

    private final ActivityFinisher activityFinisher;

    @Inject
    public DecisionCreatorActivityFinisher(ActivityFinisher activityFinisher) {
        this.activityFinisher = activityFinisher;
    }

    public void finishDecisionCreation(Decision decision) {
        Parcelable result = Parcels.wrap(Decision.class, decision);

        Intent intent = new Intent();
        intent.putExtra(CREATED_DECISION_EXTRA, result);
        activityFinisher.finishActivityWithResult(intent);
    }
}
