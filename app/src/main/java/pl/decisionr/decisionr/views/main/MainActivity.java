package pl.decisionr.decisionr.views.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.decisionr.decisionr.R;
import pl.decisionr.decisionr.core.activity.ActivityModule;
import pl.decisionr.decisionr.core.activity.security.SecuredActivity;
import pl.decisionr.decisionr.views.decision.creator.DecisionCreatorActivity;
import pl.decisionr.decisionr.views.main.decisions.my.MyDecisionsFragment;
import pl.decisionr.decisionr.views.main.decisions.other.OthersDecisionsFragment;

public class MainActivity extends SecuredActivity {

    public static final int DECISION_CREATE_REQUEST = 9987;

    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setUpTabs();
    }

    private void setUpTabs() {
        FragmentTabsAdapter adapter = new FragmentTabsAdapter(getSupportFragmentManager());
        adapter.addFragment(new MyDecisionsFragment(), "MINE");
        adapter.addFragment(new OthersDecisionsFragment(), "OTHERS");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void initializeInjector() {
        DaggerMainComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(getApplicationComponent())
                .build()
                .inject(this);
    }

    @OnClick(R.id.fab)
    void onFabClick() {
        Intent intent = new Intent(this, DecisionCreatorActivity.class);
        startActivityForResult(intent, DECISION_CREATE_REQUEST);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}