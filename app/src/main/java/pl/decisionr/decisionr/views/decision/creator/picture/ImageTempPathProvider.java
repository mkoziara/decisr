package pl.decisionr.decisionr.views.decision.creator.picture;

import android.net.Uri;

import com.annimon.stream.Optional;

public class ImageTempPathProvider {

    private Optional<Uri> firstImageUri = Optional.empty();
    private Optional<Uri> secondImageUri = Optional.empty();

    public void setFirstImageUri(Uri firstImageUri) {
        this.firstImageUri = Optional.of(firstImageUri);
    }

    public void setSecondImageUri(Uri secondImageUri) {
        this.secondImageUri = Optional.of(secondImageUri);
    }

    public Optional<Uri> getFirstImageUri() {
        return firstImageUri;
    }

    public Optional<Uri> getSecondImageUri() {
        return secondImageUri;
    }
}
