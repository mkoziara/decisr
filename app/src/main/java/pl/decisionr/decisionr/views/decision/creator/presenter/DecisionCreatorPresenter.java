package pl.decisionr.decisionr.views.decision.creator.presenter;

import android.net.Uri;

import javax.inject.Inject;

import pl.decisionr.decisionr.core.activity.ActivityState;
import pl.decisionr.decisionr.database.decision.DecisionsRepository;
import pl.decisionr.decisionr.database.decision.model.decision.DecisionState;
import pl.decisionr.decisionr.database.decision.model.decision.choice.SimpleChoiceDecision;
import pl.decisionr.decisionr.views.decision.creator.DecisionCreatorView;
import pl.decisionr.decisionr.views.decision.creator.picture.ImageTempPathProvider;
import pl.decisionr.decisionr.views.decision.creator.picture.PictureMaker;

public class DecisionCreatorPresenter {

    public static final int FIRST_IMAGE_REQUEST = 155;
    public static final int SECOND_IMAGE_REQUEST = 158;

    public static final String FIRST_URI_STATE = DecisionCreatorPresenter.class.getCanonicalName() + ":firstUri";
    public static final String SECOND_URI_STATE = DecisionCreatorPresenter.class.getCanonicalName() + ":secondUri";

    private final PictureMaker pictureMaker;
    private final DecisionFactory decisionFactory;
    private final DecisionsRepository decisionsRepository;
    private final ImageTempPathProvider imageTempPathProvider;
    private final DecisionCreatorActivityFinisher decisionCreatorActivityFinisher;
    private final DecisionCreatorView view;

    @Inject
    public DecisionCreatorPresenter(PictureMaker pictureMaker, ImageTempPathProvider imageTempPathProvider, DecisionFactory decisionFactory, DecisionsRepository decisionsRepository, DecisionCreatorActivityFinisher decisionCreatorActivityFinisher, DecisionCreatorView view) {
        this.pictureMaker = pictureMaker;
        this.decisionFactory = decisionFactory;
        this.decisionsRepository = decisionsRepository;
        this.imageTempPathProvider = imageTempPathProvider;
        this.decisionCreatorActivityFinisher = decisionCreatorActivityFinisher;
        this.view = view;
    }

    public void makeFirstPicture() {
        Uri uri = pictureMaker.makePicture(FIRST_IMAGE_REQUEST);
        imageTempPathProvider.setFirstImageUri(uri);
    }

    public void makeSecondPicture() {
        Uri uri = pictureMaker.makePicture(SECOND_IMAGE_REQUEST);
        imageTempPathProvider.setSecondImageUri(uri);
    }

    public void saveDecision(String title, DecisionState decisionState) {
        SimpleChoiceDecision simpleChoiceDecision = decisionFactory.createSimpleChoiceDecision(title, decisionState);

        decisionsRepository.saveDecision(simpleChoiceDecision)
                .subscribe(decisionCreatorActivityFinisher::finishDecisionCreation);
    }

    public ActivityState getState() {
        ActivityState state = new ActivityState();
        imageTempPathProvider.getFirstImageUri().ifPresent(u -> state.putString(FIRST_URI_STATE, u.toString()));
        imageTempPathProvider.getSecondImageUri().ifPresent(u -> state.putString(SECOND_URI_STATE, u.toString()));
        return state;
    }

    public void restoreState(ActivityState state) {
        state.getString(FIRST_URI_STATE).map(Uri::parse).ifPresent(imageTempPathProvider::setFirstImageUri);
        state.getString(SECOND_URI_STATE).map(Uri::parse).ifPresent(imageTempPathProvider::setSecondImageUri);

        imageTempPathProvider.getFirstImageUri().ifPresent(view::setFirstImage);
        imageTempPathProvider.getSecondImageUri().ifPresent(view::setSecondImage);
    }
}