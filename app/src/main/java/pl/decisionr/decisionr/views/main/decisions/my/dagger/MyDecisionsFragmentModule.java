package pl.decisionr.decisionr.views.main.decisions.my.dagger;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import pl.decisionr.decisionr.core.activity.ActivityModule;
import pl.decisionr.decisionr.core.activity.ActivityScope;
import pl.decisionr.decisionr.core.activity.result.ResultProcessor;
import pl.decisionr.decisionr.core.activity.result.ResultProcessorKey;
import pl.decisionr.decisionr.core.activity.result.ResultProcessorType;
import pl.decisionr.decisionr.core.fragment.FragmentModule;
import pl.decisionr.decisionr.views.main.decisions.my.MyDecisionsFragmentView;
import pl.decisionr.decisionr.views.main.result.MainOkResultProcessor;

@Module(includes = {FragmentModule.class, ActivityModule.class})
public class MyDecisionsFragmentModule {

    private final MyDecisionsFragmentView view;

    public MyDecisionsFragmentModule(MyDecisionsFragmentView view) {
        this.view = view;
    }

    @Provides
    @IntoMap
    @ResultProcessorKey(ResultProcessorType.OK)
    @ActivityScope
    public ResultProcessor mainOkResultProcessor() {
        return new MainOkResultProcessor(view);
    }

}
