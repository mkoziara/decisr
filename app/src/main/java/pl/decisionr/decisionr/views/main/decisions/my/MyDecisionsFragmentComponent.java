package pl.decisionr.decisionr.views.main.decisions.my;

import dagger.Component;
import pl.decisionr.decisionr.ApplicationComponent;
import pl.decisionr.decisionr.core.activity.ActivityModule;
import pl.decisionr.decisionr.core.activity.ActivityScope;
import pl.decisionr.decisionr.views.main.decisions.my.dagger.MyDecisionsFragmentModule;

@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, MyDecisionsFragmentModule.class})
@ActivityScope
public interface MyDecisionsFragmentComponent {

    void inject(MyDecisionsFragment myDecisionsFragment);

}
