package pl.decisionr.decisionr.views.main.decisions.my;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.annimon.stream.Optional;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.decisionr.decisionr.R;
import pl.decisionr.decisionr.core.Action;
import pl.decisionr.decisionr.core.activity.ActivityState;
import pl.decisionr.decisionr.core.fragment.BaseFragment;
import pl.decisionr.decisionr.database.decision.DecisionsRepository;
import pl.decisionr.decisionr.database.decision.model.decision.Decision;
import pl.decisionr.decisionr.database.decision.model.decision.DecisionComparator;
import pl.decisionr.decisionr.views.main.adapter.SimpleChoiceDecisionAdapter;
import pl.decisionr.decisionr.views.main.decisions.my.dagger.MyDecisionsFragmentModule;

public class MyDecisionsFragment extends BaseFragment implements MyDecisionsFragmentView {

    public static final String MY_DECISIONS_STATE = MyDecisionsFragment.class.getCanonicalName() + ":MyDecisionsList";

    @BindView(R.id.decisions_list)
    RecyclerView decisionRecyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.swipe_to_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @Inject
    DecisionsRepository decisionsRepository;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_decisions, container, false);
        ButterKnife.bind(this, view);

        DaggerMyDecisionsFragmentComponent
                .builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(createActivityModule())
                .myDecisionsFragmentModule(new MyDecisionsFragmentModule(this))
                .build()
                .inject(this);

        // TODO: 2017-02-22 żadna z tych operacji nie powinna się wykonać jeśli użytkownik nie jest zalogowany
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            ActivityState state = new ActivityState(savedInstanceState);
            swipeRefreshLayout.setOnRefreshListener(this::initialize);

            if (state.hasState()) {
                restoreState(state);
            } else {
                initialize();
            }
        }

        return view;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        List<Decision> decisions = ((SimpleChoiceDecisionAdapter) decisionRecyclerView.getAdapter()).getDecisions();
        ActivityState state = new ActivityState()
                .putParcel(MY_DECISIONS_STATE, decisions);

        outState.putAll(state.getBundle());
        super.onSaveInstanceState(outState);
    }

    // TODO: 2017-02-26 presenter
    // TODO: 2017-02-22 w miarę rozwijania przenieść logikę do kontrolera
    private void initialize() {
        decisionsRepository.getMyDecisions()
                .subscribe(this::initializeRecyclerView);
    }

    private void restoreState(ActivityState state) {
        Optional<List<Decision>> decisions = state.getParcel(MY_DECISIONS_STATE);
        decisions.ifPresent(this::initializeRecyclerView);
    }

    private void initializeRecyclerView(List<Decision> decisions) {
        Action dataLoadedCallback = () -> {
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            swipeRefreshLayout.setRefreshing(false);
        };
        Collections.sort(decisions, new DecisionComparator());
        SimpleChoiceDecisionAdapter decisionAdapter = new SimpleChoiceDecisionAdapter(decisions, dataLoadedCallback);

        decisionRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        decisionRecyclerView.setAdapter(decisionAdapter);
    }

    @Override
    public void addNewDecision(Decision decision) {
        // TODO: 2017-04-02 fix adding issues, nie wywołuje się resultProcessor a BaseFragment, pewnie dlatego że activity wywołuje
        SimpleChoiceDecisionAdapter adapter = (SimpleChoiceDecisionAdapter) decisionRecyclerView.getAdapter();
        adapter.addNewDecision(decision);
    }
}