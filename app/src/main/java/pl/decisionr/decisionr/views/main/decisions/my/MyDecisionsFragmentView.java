package pl.decisionr.decisionr.views.main.decisions.my;

import pl.decisionr.decisionr.database.decision.model.decision.Decision;

public interface MyDecisionsFragmentView {

    void addNewDecision(Decision decision);

}
