package pl.decisionr.decisionr.views.main.adapter;

import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.LocalTime;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.List;

import pl.decisionr.decisionr.R;
import pl.decisionr.decisionr.core.Action;
import pl.decisionr.decisionr.database.decision.model.decision.Decision;
import pl.decisionr.decisionr.database.decision.model.decision.DecisionState;
import pl.decisionr.decisionr.database.decision.model.decision.choice.SimpleChoiceDecision;

// TODO: 2017-02-05 po dodaniu elementu recycler powinien scrollować się do góry
public class SimpleChoiceDecisionAdapter extends RecyclerView.Adapter<SimpleChoiceDecisionViewHolder> {

    private final List<Decision> decisions;
    private final Action dataLoadedCallback;

    public SimpleChoiceDecisionAdapter(List<Decision> decisions, Action dataLoadedCallback) {
        this.decisions = decisions;
        this.dataLoadedCallback = dataLoadedCallback;
    }

    @Override
    public SimpleChoiceDecisionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_choice_item, parent, false);

        return new SimpleChoiceDecisionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleChoiceDecisionViewHolder viewHolder, int position) {
        dataLoadedCallback.perform();
        SimpleChoiceDecision model = (SimpleChoiceDecision) decisions.get(position);

        viewHolder.titleText.setText(model.getTitle());
        viewHolder.leftAnswersCounter.setText(model.getAnswerSummary().getLeftAnswers() + " voice(s)");
        viewHolder.rightAnswersCounter.setText(model.getAnswerSummary().getRightAnswers() + " voice(s)");
        if (model.getDecisionState().equals(DecisionState.TIME_TO_LIVE)) {
            viewHolder.decisionTimer.setVisibility(View.VISIBLE);

            new CountDownTimer(24 * 60000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    LocalTime timeToFinish = countTimeToFinish(model);
                    viewHolder.decisionTimer.setText(timeToFinish.format(DateTimeFormatter.ISO_TIME));
                }

                @Override
                public void onFinish() {
                    viewHolder.decisionTimer.setVisibility(View.GONE);
                }
            }.start();
        }

        // TODO: 2017-02-22 przydałby się na to jakiś wrapper
        StorageReference reference = FirebaseStorage.getInstance().getReference(model.getFirstImagePath());
        loadImage(reference, viewHolder.firstImageView);

        StorageReference secondReference = FirebaseStorage.getInstance().getReference(model.getSecondImagePath());
        loadImage(secondReference, viewHolder.secondImageView);
    }

    public List<Decision> getDecisions() {
        return decisions;
    }

    @Override
    public int getItemCount() {
        return decisions.size();
    }

    private LocalTime countTimeToFinish(Decision decision) {
        LocalDateTime decisionFinishTime = decision.getFinishTime();
        LocalDateTime currentTime = LocalDateTime.now();

        long hours = currentTime.until(decisionFinishTime, ChronoUnit.HOURS);
        currentTime = currentTime.plusHours(hours);

        long minutes = currentTime.until(decisionFinishTime, ChronoUnit.MINUTES);
        currentTime = currentTime.plusMinutes(minutes);

        long seconds = currentTime.until(decisionFinishTime, ChronoUnit.SECONDS);

        return LocalTime.of((int) hours, (int) minutes, (int) seconds);
    }

    // TODO: 2017-02-22 na to też jakiś wrapper
    private void loadImage(StorageReference reference, ImageView imageView) {

        List<UploadTask> activeUploadTasks = reference.getActiveUploadTasks();
        if (activeUploadTasks.isEmpty()) {
            loadImageInto(reference, imageView);
        } else {
            activeUploadTasks.get(0)
                    .addOnSuccessListener(taskSnapshot -> loadImageInto(reference, imageView));
        }
    }

    private void loadImageInto(StorageReference storageReference, ImageView imageView) {
        Glide.with(imageView.getContext())
                .using(new FirebaseImageLoader())
                .load(storageReference)
                .override(150, 150)
                .centerCrop()
                .into(imageView);
    }

    public void addNewDecision(Decision decision) {
        decisions.add(0, decision);

        notifyItemInserted(0);
        notifyItemRangeChanged(0, decisions.size());
    }
}