package pl.decisionr.decisionr.views.decision.creator.picture;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.File;

import javax.inject.Inject;

import pl.decisionr.decisionr.core.activity.ActivityStarter;

public class PictureMaker {

    private final ImagePathProvider imagePathProvider;
    private final ActivityStarter activityStarter;

    @Inject
    public PictureMaker(ImagePathProvider imagePathProvider, ActivityStarter activityStarter) {
        this.imagePathProvider = imagePathProvider;
        this.activityStarter = activityStarter;
    }

    public Uri makePicture(int requestCode) {
        File uriFile = imagePathProvider.generatePathForImage();
        Uri imageUri = Uri.fromFile(uriFile);

        Intent intent = createPictureIntent(imageUri);
        activityStarter.startActivityForResult(intent, requestCode);
        return imageUri;
    }

    private Intent createPictureIntent(Uri imageUri) {
        return new Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                .putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
    }
}
