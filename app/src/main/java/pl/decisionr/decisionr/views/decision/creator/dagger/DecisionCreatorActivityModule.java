package pl.decisionr.decisionr.views.decision.creator.dagger;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import pl.decisionr.decisionr.core.activity.ActivityModule;
import pl.decisionr.decisionr.core.activity.ActivityScope;
import pl.decisionr.decisionr.core.activity.ActivityStarter;
import pl.decisionr.decisionr.core.activity.finish.ActivityFinisher;
import pl.decisionr.decisionr.core.activity.result.ResultProcessor;
import pl.decisionr.decisionr.core.activity.result.ResultProcessorKey;
import pl.decisionr.decisionr.core.activity.result.ResultProcessorType;
import pl.decisionr.decisionr.core.paths.AndroidPathsProvider;
import pl.decisionr.decisionr.database.decision.DecisionsRepository;
import pl.decisionr.decisionr.storage.ImageStorage;
import pl.decisionr.decisionr.views.decision.creator.DecisionCreatorView;
import pl.decisionr.decisionr.views.decision.creator.picture.ImagePathProvider;
import pl.decisionr.decisionr.views.decision.creator.picture.ImageTempPathProvider;
import pl.decisionr.decisionr.views.decision.creator.picture.PictureMaker;
import pl.decisionr.decisionr.views.decision.creator.presenter.DecisionCreatorActivityFinisher;
import pl.decisionr.decisionr.views.decision.creator.presenter.DecisionCreatorPresenter;
import pl.decisionr.decisionr.views.decision.creator.presenter.DecisionFactory;
import pl.decisionr.decisionr.views.decision.creator.result.DecisionCreatorOkResultProcessor;

// TODO: 2017-02-22 przeczyścić te providesy, nie zawsze są potrzebne
@Module(includes = ActivityModule.class)
public class DecisionCreatorActivityModule {

    private final DecisionCreatorView view;

    public DecisionCreatorActivityModule(DecisionCreatorView view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    public DecisionCreatorView providesDecisionCreatorView() {
        return view;
    }

    @Provides
    @ActivityScope
    public DecisionCreatorPresenter providesDecisionCreatorPresenter(PictureMaker pictureMaker, ImageTempPathProvider imageTempPathProvider, DecisionFactory decisionFactory, DecisionsRepository decisionsRepository, DecisionCreatorActivityFinisher activityFinisher) {
        return new DecisionCreatorPresenter(pictureMaker, imageTempPathProvider, decisionFactory, decisionsRepository, activityFinisher, view);
    }

    @Provides
    @ActivityScope
    public DecisionFactory providesDecisionFactory(ImageStorage imageStorage, ImageTempPathProvider imageTempPathProvider) {
        return new DecisionFactory(imageStorage, imageTempPathProvider);
    }

    @Provides
    @ActivityScope
    public DecisionCreatorActivityFinisher proideDecisionCreatorActivityFinisher(ActivityFinisher activityFinisher) {
        return new DecisionCreatorActivityFinisher(activityFinisher);
    }

    @Provides
    @ActivityScope
    public ImageTempPathProvider providesImageTempPathProvider() {
        return new ImageTempPathProvider();
    }

    @Provides
    @ActivityScope
    public PictureMaker providesPictureMaker(ImagePathProvider imagePathProvider, ActivityStarter activityStarter) {
        return new PictureMaker(imagePathProvider, activityStarter);
    }

    @Provides
    @ActivityScope
    public ImagePathProvider providesImagePathProvider(AndroidPathsProvider pathsProvider) {
        return new ImagePathProvider(pathsProvider);
    }

    @Provides
    @IntoMap
    @ResultProcessorKey(ResultProcessorType.OK)
    @ActivityScope
    public ResultProcessor providesResultProcessor(ImageTempPathProvider imageTempPathProvider) {
        return new DecisionCreatorOkResultProcessor(view, imageTempPathProvider);
    }
}
