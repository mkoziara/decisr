package pl.decisionr.decisionr.views.signin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ResultCodes;

import java.util.Collections;

import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.decisionr.decisionr.R;
import pl.decisionr.decisionr.core.activity.BaseActivity;
import pl.decisionr.decisionr.views.main.MainActivity;

public class SignInActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
    }

    @Override
    protected void initializeInjector() {

    }

    @OnClick(R.id.sign_in_button)
    public void onSignInClick() {
        startActivityForResult(AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setProviders(Collections.singletonList(new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build()))
                .build(), 1992);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1992) {
            if (resultCode == ResultCodes.OK) {
                startActivity(new Intent(this, MainActivity.class));
                finish();
            } else {
                throw new RuntimeException("Authentication failed!");
            }
        }
    }
}
