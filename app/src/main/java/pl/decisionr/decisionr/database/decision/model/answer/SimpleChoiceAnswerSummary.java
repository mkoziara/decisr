package pl.decisionr.decisionr.database.decision.model.answer;

import org.parceler.Parcel;

@Parcel
public class SimpleChoiceAnswerSummary {

    long leftAnswers;
    long rightAnswers;

    public long getLeftAnswers() {
        return leftAnswers;
    }

    public void setLeftAnswers(long leftAnswers) {
        this.leftAnswers = leftAnswers;
    }

    public long getRightAnswers() {
        return rightAnswers;
    }

    public void setRightAnswers(long rightAnswers) {
        this.rightAnswers = rightAnswers;
    }
}
