package pl.decisionr.decisionr.database.decision.model.decision;

import java.util.Comparator;

public class DecisionComparator implements Comparator<Decision> {

    @Override
    public int compare(Decision o1, Decision o2) {

        DecisionState firstState = o1.getDecisionState();
        DecisionState secondState = o2.getDecisionState();

        if(firstState.equals(DecisionState.FINISHED)) {
            if (secondState.equals(DecisionState.FINISHED)) {
                return 0;
            }

            return 1;
        }

        if(firstState.equals(DecisionState.INFINITE_TIME)) {
            if (secondState.equals(DecisionState.TIME_TO_LIVE)) {
                return 1;
            }
            if (secondState.equals(DecisionState.FINISHED)) {
                return -1;
            }
        }

        if (firstState.equals(DecisionState.TIME_TO_LIVE)) {
            if (secondState.equals(DecisionState.TIME_TO_LIVE)) {
                return o1.getFinishTime().compareTo(o2.getFinishTime());
            }

            return -1;
        }

        return 0;
    }
}
