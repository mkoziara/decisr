package pl.decisionr.decisionr.database.decision.model.answer;

import pl.decisionr.decisionr.database.decision.model.decision.DecisionType;

public abstract class DecisionAnswer {


    // TODO: 2017-02-22 moze immutable
    private String answeredBy;

    public String getAnsweredBy() {
        return answeredBy;
    }

    public void setAnsweredBy(String answeredBy) {
        this.answeredBy = answeredBy;
    }

    public abstract DecisionType getDecisionAnswerType();
}
