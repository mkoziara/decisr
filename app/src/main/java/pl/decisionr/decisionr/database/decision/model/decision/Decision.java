package pl.decisionr.decisionr.database.decision.model.decision;

import org.parceler.Parcel;
import org.threeten.bp.LocalDateTime;

import pl.decisionr.decisionr.database.decision.converters.ParcelDecisionConverter;

@Parcel(converter = ParcelDecisionConverter.class)
public abstract class Decision {

    String title;
    String id;
    LocalDateTime finishTime;
    DecisionState decisionState;

    // TODO: 2017-02-22 może dałoby radę zrobić niemutowalny konstruktor
    public Decision() {
    }

    protected Decision(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DecisionState getDecisionState() {
        return decisionState;
    }

    public void setDecisionState(DecisionState decisionState) {
        this.decisionState = decisionState;
    }

    public LocalDateTime getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(LocalDateTime finishTime) {
        this.finishTime = finishTime;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public abstract DecisionType getType();
}
