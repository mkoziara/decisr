package pl.decisionr.decisionr.database;

import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.decisionr.decisionr.auth.FirebaseAuthModule;
import pl.decisionr.decisionr.database.decision.DecisionsRepository;
import pl.decisionr.decisionr.rest.DecisionsClient;

@Module(includes = FirebaseAuthModule.class)
public class DatabaseModule {

    @Provides
    @Singleton
    public DecisionsRepository providesDecisionRepository(DecisionsClient decisionsClient) {
        return new DecisionsRepository(decisionsClient);
    }

    @Provides
    @Singleton
    public FirebaseDatabase providesFirebaseDatabase() {
        return FirebaseDatabase.getInstance();
    }
}
