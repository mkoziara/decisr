package pl.decisionr.decisionr.database.decision.model.decision;

public enum DecisionState {
    TIME_TO_LIVE, INFINITE_TIME, FINISHED
}
