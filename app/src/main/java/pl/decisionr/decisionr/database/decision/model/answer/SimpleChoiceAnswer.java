package pl.decisionr.decisionr.database.decision.model.answer;

import pl.decisionr.decisionr.database.decision.model.decision.DecisionType;

public class SimpleChoiceAnswer extends DecisionAnswer {

    public static final String RIGHT = "RIGHT";
    public static final String LEFT = "LEFT";

    private String answer;

    public static SimpleChoiceAnswer createLeft() {
        return new SimpleChoiceAnswer(LEFT);
    }

    public static SimpleChoiceAnswer createRight() {
        return new SimpleChoiceAnswer(RIGHT);
    }

    public SimpleChoiceAnswer() {
    }

    public SimpleChoiceAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public DecisionType getDecisionAnswerType() {
        return DecisionType.SIMPLE_CHOICE;
    }
}