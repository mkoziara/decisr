package pl.decisionr.decisionr.database.decision.converters;

import android.os.Parcel;

import org.parceler.ParcelConverter;
import org.parceler.Parcels;

import pl.decisionr.decisionr.database.decision.model.decision.Decision;

public class ParcelDecisionConverter implements ParcelConverter<Decision> {

    @Override
    public void toParcel(Decision input, Parcel parcel) {
        if (input == null ) {
            parcel.writeInt(-1);
        } else {
            parcel.writeParcelable(Parcels.wrap(input), 0);
        }
    }

    @Override
    public Decision fromParcel(Parcel parcel) {
        return Parcels.unwrap(parcel.readParcelable(Decision.class.getClassLoader()));
    }
}