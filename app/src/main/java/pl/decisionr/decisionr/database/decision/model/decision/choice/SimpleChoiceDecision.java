package pl.decisionr.decisionr.database.decision.model.decision.choice;

import org.parceler.Parcel;

import pl.decisionr.decisionr.database.decision.model.answer.SimpleChoiceAnswerSummary;
import pl.decisionr.decisionr.database.decision.model.decision.Decision;
import pl.decisionr.decisionr.database.decision.model.decision.DecisionType;

@Parcel
public class SimpleChoiceDecision extends Decision {

    String firstImagePath;
    String secondImagePath;
    SimpleChoiceAnswerSummary answerSummary;

    public SimpleChoiceDecision() {
        super();
    }

    public SimpleChoiceDecision(String title, String firstImagePath, String secondImagePath) {
        super(title);
        this.firstImagePath = firstImagePath;
        this.secondImagePath = secondImagePath;
    }

    public SimpleChoiceAnswerSummary getAnswerSummary() {
        return answerSummary;
    }

    public void setAnswerSummary(SimpleChoiceAnswerSummary answerSummary) {
        this.answerSummary = answerSummary;
    }

    public String getFirstImagePath() {
        return firstImagePath;
    }

    public void setFirstImagePath(String firstImagePath) {
        this.firstImagePath = firstImagePath;
    }

    public String getSecondImagePath() {
        return secondImagePath;
    }

    public void setSecondImagePath(String secondImagePath) {
        this.secondImagePath = secondImagePath;
    }

    @Override
    public DecisionType getType() {
        return DecisionType.SIMPLE_CHOICE;
    }
}
