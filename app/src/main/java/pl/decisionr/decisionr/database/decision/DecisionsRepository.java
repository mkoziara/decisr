package pl.decisionr.decisionr.database.decision;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import pl.decisionr.decisionr.database.decision.model.answer.DecisionAnswer;
import pl.decisionr.decisionr.database.decision.model.decision.Decision;
import pl.decisionr.decisionr.rest.DecisionsClient;

public class DecisionsRepository {

    private final DecisionsClient decisionsClient;

    @Inject
    public DecisionsRepository(DecisionsClient decisionsClient) {
        this.decisionsClient = decisionsClient;
    }

    public Observable<List<Decision>> getMyDecisions() {
        return decisionsClient.getMyDecisions()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    public Observable<Decision> saveDecision(Decision decision) {
        return decisionsClient.createNewDecision(decision)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    public Observable<List<Decision>> getOtherDecisions() {
        return decisionsClient.getOtherDecisions()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    public Completable addAnswerToDecision(Decision decision, DecisionAnswer answer) {
        return decisionsClient.addAnswer(decision.getId(), answer)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}