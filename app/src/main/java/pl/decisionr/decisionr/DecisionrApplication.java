package pl.decisionr.decisionr;

import android.app.Application;

import com.jakewharton.threetenabp.AndroidThreeTen;

import pl.decisionr.decisionr.core.ApplicationModule;


public class DecisionrApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        AndroidThreeTen.init(this);
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
